#ifndef hero_H
#define hero_H

#include <QObject>
#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsScene>
#include <Qt>
#include <QKeyEvent>
#include <QDebug>
#include <QMediaPlayer>
#include <QMediaPlaylist>

// Наследуемся от QGraphicsItem
class Hero : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
public:
    explicit Hero(QObject *parent = 0);

    ~Hero();
    QMediaPlayer * m_player;        // Аудио плеер
    QMediaPlaylist * m_playlist;    // Плейлист
void plusAngle(qreal i)
{
    angle+=i;
}

qreal getAngle()
{
    return angle;
}

int getCountForSteps()
{
    return countForSteps;
}
void setCountForSteps(int i)
{
    if(i!=0) countForSteps+=i;
        else countForSteps=0;
}

void setSteps(int i)
{
    steps=i;
}
signals:
    /* Сигнал, который передаётся в ядро игры с элементом QGraphicsItem,
     * на который наткнулась муха, и требуется принять решение о том,
     * что с этим элементом делать.
     * */
    void signalCheckItem(QGraphicsItem *item, int i = 0);

protected:
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private:
    qreal angle;    // Угол поворота графического объекта
    int steps;          // Номер положения ножек мухи
    int countForSteps;  // Счётчик для отсчета тиков таймера, при которых мы нажимали на кнопки

};

#endif // hero_H
