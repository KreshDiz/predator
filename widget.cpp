#include "widget.h"
#include "ui_widget.h"
#include "hero.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    //this->resize(600,640);          /// Задаем размеры виджета, то есть окна
    //this->setFixedSize(600,640);    /// Фиксируем размеры виджета

    count = 0;

    scene = new QGraphicsScene();   /// Инициализируем графическую сцену


    ui->graphicsView->setScene(scene);  /// Устанавливаем графическую сцену в graphicsView
    ui->graphicsView->setRenderHint(QPainter::Antialiasing);    /// Устанавливаем сглаживание
    ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff); /// Отключаем скроллбар по вертикали
    ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff); /// Отключаем скроллбар по горизонтали

    scene->setSceneRect(-250,-250,500,500); /// Устанавливаем область графической сцены

    one_timer = new QTimer();
    timerCreatePrey = new QTimer();

    gameState = GAME_STOPED;

    pauseKey = new QShortcut(this);
    pauseKey->setKey(Qt::Key_Pause);
    connect(pauseKey, &QShortcut::activated, this, &Widget::on_pushButton_Pause_clicked);

}


void Widget::slotDeletePrey(QGraphicsItem *item, int i)
{
    /* Получив сигнал от Мухи
     * Перебираем весь список яблок и удаляем найденное яблоко
     * */
    foreach (QGraphicsItem *Prey, Preys) {
        if(Prey == item){
            scene->removeItem(Prey);   // Удаляем со сцены
            Preys.removeOne(item);     // Удаляем из списка
            delete Prey;               // Вообще удаляем
            if(i==1)
                ui->lcdNumber->display(++count);
            else
                ui->lcdNumber_2->display(++count2);
            QMediaPlayer * m_player = new QMediaPlayer(this);   // Инициализируем плеер
            QMediaPlaylist * m_playlist = new QMediaPlaylist(m_player); // Создаём плейлист

           m_player->setPlaylist(m_playlist);  // Устанавливаем плейлист в плеер
           m_playlist->addMedia(QUrl("qrc:/game/hrum.wav")); // Добавляем аудио в плеер
           m_playlist->setPlaybackMode(QMediaPlaylist::CurrentItemOnce); // Проигрываем один раз
           m_player->play();   // Запускаем плеер
        }
    }
}

void Widget::slotCreatePrey()
{
    Prey *prey = new Prey(); // Создаём яблоко
    scene->addItem(prey);      // Помещаем его в сцену со случайной позицией
    prey->setPos((qrand() % (331)) * ((qrand()%2 == 1)?1:-1),
                  (qrand() % (331)) * ((qrand()%2 == 1)?1:-1));
    prey->setZValue(-1);       /* Помещаем яблоко ниже Мухи, то есть Муха
                                 * на сцене будет выше яблок
                                 * */
    Preys.append(prey);       // Добавляем Муху в Список
}

void Widget::keyPressEvent(QKeyEvent *event)
{
    qDebug()<<"Press"<<event->key();
    switch (event->key()) {
    case Qt::Key_A:
        mykey->keyLeft=1;
        break;
    case Qt::Key_D:
        mykey->keyRight=1;
        break;
    case Qt::Key_W:
        mykey->keyUp=1;
        break;
    case Qt::Key_S:
        mykey->keyDown=1;
        break;
    case 1060:
        mykey->keyLeft=1;
        break;
    case 1042:
        mykey->keyRight=1;
        break;
    case 1062:
        mykey->keyUp=1;
        break;
    case 1067:
        mykey->keyDown=1;
        break;
    case Qt::Key_J:
        mykey->keyj=1;
        break;
    case Qt::Key_L:
        mykey->keyl=1;
        break;
    case Qt::Key_I:
        mykey->keyi=1;
        break;
    case Qt::Key_K:
        mykey->keyk=1;
        break;
    default:
        break;
    }

}

void Widget::keyReleaseEvent(QKeyEvent *event) {
     qDebug()<<"Release"<<event->key();
     switch (event->key()) {
     case Qt::Key_A:
         mykey->keyLeft=0;
         break;
     case Qt::Key_D:
         mykey->keyRight=0;
         break;
     case Qt::Key_W:
         mykey->keyUp=0;
         break;
     case Qt::Key_S:
         mykey->keyDown=0;
         break;
     case 1060:
         mykey->keyLeft=0;
         break;
     case 1042:
         mykey->keyRight=0;
         break;
     case 1062:
         mykey->keyUp=0;
         break;
     case 1067:
         mykey->keyDown=0;
         break;
     case Qt::Key_J:
         mykey->keyj=0;
         break;
     case Qt::Key_L:
         mykey->keyl=0;
         break;
     case Qt::Key_I:
         mykey->keyi=0;
         break;
     case Qt::Key_K:
         mykey->keyk=0;
         break;
     default:
         break;
     }
}
void Widget::onkey()
{
    if(mykey->keyLeft==1){
        hero->plusAngle(-5);        // Задаём поворот на 10 градусов влево
        hero->setRotation(hero->getAngle()); // Поворачиваем объект
    }
    if( mykey->keyRight==1){
        hero->plusAngle(5);        // Задаём поворот на 10 градусов вправо
        hero->setRotation(hero->getAngle()); // Поворачиваем объект
    }
    if( mykey->keyUp==1){
        hero->setPos(hero->mapToParent(0, -2));     /* Продвигаем объект на 5 пискселей вперёд
                                         * перетранслировав их в координатную систему
                                         * графической сцены
                                         * */
    }
    //if( mykey->keyDown==1){
      //  hero->setPos(hero->mapToParent(0, 2));      /* Продвигаем объект на 5 пискселей назад
     //                                    * перетранслировав их в координатную систему
     //                                    * графической сцены
    //                                     * */
   // }
      if(mykey->keyLeft==1 || mykey->keyRight==1 || mykey->keyUp==1)
      {
           hero->setCountForSteps(1);
           if(hero->getCountForSteps() == 4){
               hero->setSteps(2);
               hero->update(QRectF(-40,-50,80,100));
           } else if (hero->getCountForSteps() == 8){
               hero->setSteps(1);
               hero->update(QRectF(-40,-50,80,100));
           } else if (hero->getCountForSteps() == 12){
               hero->setSteps(0);
               hero->update(QRectF(-40,-50,80,100));
           } else if (hero->getCountForSteps() == 16) {
               hero->setSteps(1);
               hero->update(QRectF(-40,-50,80,100));
               hero->setCountForSteps(0);
           }
       }

      if(mykey->keyLeft==1 || mykey->keyRight==1 || mykey->keyUp==1)
      hero->m_player->play();   // Плеер играет только тогда, когда муха движется
      else {
      hero-> m_player->stop();   // Если Муха не движется, то отключаем плее
  }


    /* Проверяем, нажата ли была какая-либо из кнопок управления объектом.
         * Прежде чем считать шажки
         * */

        /* Производим проверку на то, наткнулась ли муха на какой-нибудь
         * элемент на графической сцене.
         * Для этого определяем небольшую область перед мухой,
         * в которой будем искать элементы
         * */
        QList<QGraphicsItem *> foundItems = hero->scene()->items(QPolygonF()
                                                               << hero->mapToScene(0, 0)
                                                               << hero->mapToScene(-20, -20)
                                                               << hero->mapToScene(20, -20));
        /* После чего проверяем все элементы.
         * Один из них будет сама Муха - с ней ничего не делаем.
         * А с остальными высылаем сигнал в ядро игры
         * */
        foreach (QGraphicsItem *item, foundItems) {
            if (item == hero)
                continue;
            emit hero->signalCheckItem(item,1);
        }

        /* Проверка выхода за границы поля
         * Если объект выходит за заданные границы, то возвращаем его назад
         * */

    /* Проверка выхода за границы поля
     * Если объект выходит за заданные границы, то возвращаем его назад
     * */
    if(hero->x() - 10 < -400){
        hero->setX(-390);       // слева
    }
    if(hero->x() + 10 > 400){
        hero->setX(390);        // справа
    }

    if(hero->y() - 10 < -330){
        hero->setY(-320);       // сверху
    }
    if(hero->y() + 10 > 330){
        hero->setY(320);        // снизу
    }
}

void Widget::on_pushButton_clicked()
{
    count = 0;
    ui->lcdNumber->display(count);
    hero = new Hero();      /// Инициализируем муху

    scene->addItem(hero);       /// Добавляем на сцену треугольник
    hero->setPos(0,0);          /// Устанавливаем треугольник в центр сцены
    mykey = new key();
    predator = new Predator(hero);  // Инициализируем паука
    scene->addItem(predator);         // Добавляем паука на сцену
    predator->setPos(180,180);        // Устанавливаем позицию паука

    /* Подключаем сигнал от паука на проверку состояния GameOver
     * */
    connect(predator, &Predator::signalCheckGameOver, this, &Widget::slotGameOver);

      connect(one_timer, &QTimer::timeout, this, &Widget::onkey);

      one_timer->start(10);
      timerCreatePrey->start(1000);

    connect(timerCreatePrey, &QTimer::timeout, this, &Widget::slotCreatePrey);



    connect(hero, &Hero::signalCheckItem, this, &Widget::slotDeletePrey);

    ui->pushButton->setEnabled(false);
    ui->pushButton_2->setEnabled(false);

    gameState = GAME_STARTED;
}

void Widget::slotGameOver()
{
    if(one_timer->isActive())
        one_timer->stop();
    timerCreatePrey->stop();
    QMediaPlayer * m_player = new QMediaPlayer(this);       // Создаём плеер
    QMediaPlaylist * m_playlist = new QMediaPlaylist(m_player); // Создаём плейлист

    m_player->setPlaylist(m_playlist);      // Устанавливаем плейлист в плеер
    m_playlist->addMedia(QUrl("qrc:/game/scream.wav")); // Добавляем аудио в плейлист
    m_playlist->setPlaybackMode(QMediaPlaylist::CurrentItemOnce);   // Проигрываем трек один раз
    //m_player->play();   // Запускаем трек
    QMessageBox::warning(this,
                         "Game Over",
                         "Мои соболезнования, но Вас cъели");
    /* Отключаем все сигналы от слотов
     * */
    disconnect(one_timer, &QTimer::timeout, this, &Widget::onkey);
    disconnect(timerCreatePrey, &QTimer::timeout, this, &Widget::slotCreatePrey);
    disconnect(hero, &Hero::signalCheckItem, this, &Widget::slotDeletePrey);
    disconnect(predator, &Predator::signalCheckGameOver, this, &Widget::slotGameOver);
    predator->deleteLater();
    hero->deleteLater();
    mykey->deleteLater();

    foreach (QGraphicsItem *Prey, Preys) {
        scene->removeItem(Prey);   // Удаляем со сцены
        Preys.removeOne(Prey);    // Удаляем из списка
        delete Prey;               // Вообще удаляем
    }
    /* Активируем кнопку старта игры
     * */
    ui->pushButton->setEnabled(true);
    ui->pushButton_2->setEnabled(true);

    gameState = GAME_STOPED; // Устанавливаем состояние игры в GAME_STOPED
}


Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_Pause_clicked()
{
    if(gameState == GAME_STARTED){
        if(one_timer->isActive()){
            one_timer->stop();
            timerCreatePrey->stop();
            predator->pause();
        } else {
            one_timer->start(10);
            timerCreatePrey->start(1000);
            predator->pause();
        }}
}

void Widget::on_pushButton_2_clicked()
{
    count = 0;
    count2 = 0;
    ui->lcdNumber->display(count);
    ui->lcdNumber_2->display(count);
    hero = new Hero();      /// Инициализируем муху
    hero2 = new Hero();

    scene->addItem(hero);       /// Добавляем на сцену треугольник
    scene->addItem(hero2);
    hero->setPos(0,0);
    hero2->setPos(20,20);
    mykey = new key();
    predator = new Predator(hero, hero2);  // Инициализируем паука
    scene->addItem(predator);         // Добавляем паука на сцену
    predator->setPos(180,180);        // Устанавливаем позицию паука

    /* Подключаем сигнал от паука на проверку состояния GameOver
     * */
    connect(predator, &Predator::signalCheckGameOver2, this, &Widget::slotGameOver2);

      connect(one_timer, &QTimer::timeout, this, &Widget::onkey);
      connect(one_timer, &QTimer::timeout, this, &Widget::onkey2);

      one_timer->start(10);
      timerCreatePrey->start(1000);

    connect(timerCreatePrey, &QTimer::timeout, this, &Widget::slotCreatePrey);



    connect(hero, &Hero::signalCheckItem, this, &Widget::slotDeletePrey);
    connect(hero2, &Hero::signalCheckItem, this, &Widget::slotDeletePrey);

    ui->pushButton->setEnabled(false);
    ui->pushButton_2->setEnabled(false);

    gameState = GAME_STARTED;
}

void Widget::onkey2()
{
    if(mykey->keyj==1){
        hero2->plusAngle(-5);        // Задаём поворот на 10 градусов влево
        hero2->setRotation(hero2->getAngle()); // Поворачиваем объект
    }
    if( mykey->keyl==1){
        hero2->plusAngle(5);        // Задаём поворот на 10 градусов вправо
        hero2->setRotation(hero2->getAngle()); // Поворачиваем объект
    }
    if( mykey->keyi==1){
        hero2->setPos(hero2->mapToParent(0, -2));     /* Продвигаем объект на 5 пискселей вперёд
                                         * перетранслировав их в координатную систему
                                         * графической сцены
                                         * */
    }
    //if( mykey->keyk==1){
      //  hero2->setPos(hero2->mapToParent(0, 2));      /* Продвигаем объект на 5 пискселей назад
     //                                    * перетранслировав их в координатную систему
     //                                    * графической сцены
    //                                     * */
   // }
      if(mykey->keyj==1 || mykey->keyl==1 || mykey->keyi==1)
      {
           hero2->setCountForSteps(1);
           if(hero2->getCountForSteps() == 4){
               hero2->setSteps(2);
               hero2->update(QRectF(-40,-50,80,100));
           } else if (hero2->getCountForSteps() == 8){
               hero2->setSteps(1);
               hero2->update(QRectF(-40,-50,80,100));
           } else if (hero2->getCountForSteps() == 12){
               hero2->setSteps(0);
               hero2->update(QRectF(-40,-50,80,100));
           } else if (hero2->getCountForSteps() == 16) {
               hero2->setSteps(1);
               hero2->update(QRectF(-40,-50,80,100));
               hero2->setCountForSteps(0);
           }
       }

      if(mykey->keyj==1 || mykey->keyl==1 || mykey->keyi==1)
      hero2->m_player->play();   // Плеер играет только тогда, когда муха движется
      else {
      hero2-> m_player->stop();   // Если Муха не движется, то отключаем плее
  }


    /* Проверяем, нажата ли была какая-либо из кнопок управления объектом.
         * Прежде чем считать шажки
         * */

        /* Производим проверку на то, наткнулась ли муха на какой-нибудь
         * элемент на графической сцене.
         * Для этого определяем небольшую область перед мухой,
         * в которой будем искать элементы
         * */
        QList<QGraphicsItem *> foundItems = hero2->scene()->items(QPolygonF()
                                                               << hero2->mapToScene(0, 0)
                                                               << hero2->mapToScene(-20, -20)
                                                               << hero2->mapToScene(20, -20));
        /* После чего проверяем все элементы.
          А с остальными высылаем сигнал в ядро игры
         */
        foreach (QGraphicsItem *item, foundItems) {
            if (item == hero2)
                continue;
            emit hero2->signalCheckItem(item, 2);
        }

        /* Проверка выхода за границы поля
         * Если объект выходит за заданные границы, то возвращаем его назад
         * */

    /* Проверка выхода за границы поля
     * Если объект выходит за заданные границы, то возвращаем его назад
     * */
    if(hero2->x() - 10 < -400){
        hero2->setX(-390);       // слева
    }
    if(hero2->x() + 10 > 400){
        hero2->setX(390);        // справа
    }

    if(hero2->y() - 10 < -330){
        hero2->setY(-320);       // сверху
    }
    if(hero2->y() + 10 > 330){
        hero2->setY(320);        // снизу
    }
}

void Widget::slotGameOver2()
{
    if(one_timer->isActive())
        one_timer->stop();
    timerCreatePrey->stop();
    QMediaPlayer * m_player = new QMediaPlayer(this);       // Создаём плеер
    QMediaPlaylist * m_playlist = new QMediaPlaylist(m_player); // Создаём плейлист

    m_player->setPlaylist(m_playlist);      // Устанавливаем плейлист в плеер
    m_playlist->addMedia(QUrl("qrc:/game/scream.wav")); // Добавляем аудио в плейлист
    m_playlist->setPlaybackMode(QMediaPlaylist::CurrentItemOnce);   // Проигрываем трек один раз
    //m_player->play();   // Запускаем трек
    if(count>count2)
        QMessageBox::information(0, "Игра закончена","Победил Игрок 1");
    else
         if(count<count2)
            QMessageBox::information(0, "Игра закончена","Победил Игрок 1");
        else
             QMessageBox::information(0, "Игра закончена","Ничья!");
    //QMessageBox::warning(this, "Game Over", "Мои соболезнования, но Вас cъели");


    disconnect(one_timer, &QTimer::timeout, this, &Widget::onkey);
    disconnect(one_timer, &QTimer::timeout, this, &Widget::onkey2);
    disconnect(timerCreatePrey, &QTimer::timeout, this, &Widget::slotCreatePrey);
    disconnect(hero, &Hero::signalCheckItem, this, &Widget::slotDeletePrey);
    disconnect(hero2, &Hero::signalCheckItem, this, &Widget::slotDeletePrey);
    disconnect(predator, &Predator::signalCheckGameOver2, this, &Widget::slotGameOver2);
    predator->deleteLater();
    hero->deleteLater();
    hero2->deleteLater();
    mykey->deleteLater();

    foreach (QGraphicsItem *Prey, Preys) {
        scene->removeItem(Prey);   // Удаляем со сцены
        Preys.removeOne(Prey);    // Удаляем из списка
        delete Prey;               // Вообще удаляем
    }
    /* Активируем кнопку старта игры
     * */
    ui->pushButton->setEnabled(true);
    ui->pushButton_2->setEnabled(true);

    gameState = GAME_STOPED; // Устанавливаем состояние игры в GAME_STOPED
}
