#ifndef KEY_H
#define KEY_H
#include <QObject>


class key: public QObject
{
Q_OBJECT
public:
    explicit key(QObject *parent = 0);
    int keyLeft;
    int keyRight;
    int keyUp;
    int keyDown;
    int keyi;
    int keyk;
    int keyj;
    int keyl;

    void cleanKey()
    {
        keyLeft=0;
        keyRight=0;
        keyUp=0;
        keyDown=0;
        keyi=0;
        keyk=0;
        keyj=0;
        keyl=0;
    }
    ~key();
};

#endif // KEY_H
