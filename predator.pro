#-------------------------------------------------
#
# Project created by QtCreator 2016-02-08T14:37:52
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = salki
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    hero.cpp \
    prey.cpp \
    key.cpp \
    predator.cpp

HEADERS  += widget.h \
    hero.h \
    prey.h \
    key.h \
    predator.h

FORMS    += widget.ui

DISTFILES +=

RESOURCES += \
    resources/game.qrc
