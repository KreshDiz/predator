#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QShortcut>
#include <QDebug>
#include <QTimer>
#include <QMessageBox>
#include <QMediaPlayer>
#include <QMediaPlaylist>

#include <hero.h>
#include <prey.h>
#include <predator.h>
#include <key.h>

#define GAME_STOPED 0
#define GAME_STARTED 1

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    void keyReleaseEvent(QKeyEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void onkey();
    void onkey2();
private:
    Ui::Widget      *ui;
    QGraphicsScene  *scene;
    Hero        *hero;
    Hero        *hero2;
    QTimer          *one_timer;
    QTimer          *timerCreatePrey;

    QList<QGraphicsItem *> Preys;  // Список со всеми яблоками, присутствующими в игре
    double count;   // Переменная, которая хранит счёт игре
    double count2;
    key *mykey;
    Predator          *predator;

    QShortcut       *pauseKey;      // Горячая клавиша, отвечающая за паузу в игре
    int             gameState;      /* Переменная, которая хранит состояние игры.
                                         * То есть, если игра запущена, то статус будет GAME_STARTED,
                                         * в противном случае GAME_STOPED
                                         * */

private slots:
    // Слот для удаления яблок если Муха наткнулая на это яблоко
    void slotDeletePrey( QGraphicsItem * item, int i=0);
    void slotCreatePrey();     // Слот для создания яблок, срабатывает по таймеру
    void on_pushButton_clicked();               // Слот для запуска игры
    void slotGameOver();
    void slotGameOver2();
    void on_pushButton_Pause_clicked();
    void on_pushButton_2_clicked();
};

#endif // WIDGET_H
