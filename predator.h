#ifndef predator_H
#define predator_H


#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainter>
#include <QTimer>
#include <QDebug>

class Predator : public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    explicit Predator(QGraphicsItem * target, QGraphicsItem * target2=0, QObject *parent = 0);
    ~Predator();
    void pause();   // Сигнал для инициализации паузы

signals:
    void signalCheckGameOver();  // Сигнал на вызов состояния Game Over
    void signalCheckGameOver2();

public slots:

protected:
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private:
    qreal angle;        // Угол поворота графического объекта
    int steps;          // Номер положения ножек паука
    int countForSteps;  // Счетчик для изменения полоэения ножек
    QTimer      *timer;     // Внутренний таймер паука, по которому инициализируется его движение
    QGraphicsItem * target; // Цель паука, данный объект приравнивается объекту Мухи
    QGraphicsItem * target2;

private slots:
    void slotGameTimer();   // Слот игрового таймера паука
};

#endif // predator_H
