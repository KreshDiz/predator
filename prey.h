#ifndef PreyH
#define PreyH

#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainter>

class Prey: public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    explicit Prey(QObject *parent = 0);
    ~Prey();

signals:

public slots:

protected:
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private:
    int color;
};


#endif // PreyH
